//
//  ViewController.swift
//  DemoApp
//
//  Created by Krowdforce on 01/03/19.
//  Copyright © 2019 Krowdforce. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Home"
        self.navigationItem.hidesBackButton = true
        print(Date())
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let date = dateFormatter.date(from: "2017-01-09T11:00:00.000Z")
        print("date: \(date)")
        let str = date?.timeAgoDisplay()
        print(str)
        
        print(2.square())
        // Do any additional setup after loading the view, typically from a nib.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         self.navigationItem.title = "Home123"
    }
    @IBAction func NextBtnAction(_ sender: UIButton) {
        performSegue(withIdentifier: "nextvc", sender: nil)
    }
     func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "nextvc"{
            let abcViewController = segue.destination as? NextVC
        abcViewController?.title = "Next"
    }
  }
    
}

extension Date{
    func timeAgoDisplay() -> String {
        let secondsAgo = Int(Date().timeIntervalSince(self))
        let minute = 60
        let hour =  60 * minute
        let day = 24 * hour
        let week = 7 * day
        let month = 4 * week
        
        let quoient: Int
        let unit: String
        if secondsAgo < minute{
            quoient = secondsAgo
            unit = "second"
        }else if secondsAgo < hour{
            quoient = secondsAgo/minute
            unit = "min"
        }else if secondsAgo < day{
            quoient = secondsAgo/hour
            unit = "hour"
        }else if secondsAgo < week{
            quoient = secondsAgo/day
            unit = "day"
        }else if secondsAgo < month{
            quoient = secondsAgo/month
            unit = "week"
        }else{
            quoient = secondsAgo/month
            unit = "month"
        }
        
        return "\(quoient) \(unit)\(quoient == 1 ? "":"s") ago"
        
    }
}

extension Int{
    func square() -> Int{
        
        return self * self
    }
}

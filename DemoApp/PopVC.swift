//
//  PopVC.swift
//  DemoApp
//
//  Created by Krowdforce on 01/03/19.
//  Copyright © 2019 Krowdforce. All rights reserved.
//

import UIKit

class PopVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func ClosePopAction(_ sender: Any) {
//        for controller in self.navigationController!.viewControllers as Array {
//            if controller.isKind(of: ViewController.self) {
//                _ =  self.navigationController!.popToViewController(controller, animated: true)
//                break
//            }
//        }
         //performSegue(withIdentifier: "main", sender: nil)
        let secondViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ViewController") as! ViewController
        self.navigationController?.pushViewController(secondViewController, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "main"{
            //dismiss(animated: true, completion: nil)
            let abcViewController = segue.destination as? ViewController
            abcViewController?.title = "Home New"
        }
    }
    

}

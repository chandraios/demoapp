//
//  NextVC.swift
//  DemoApp
//
//  Created by Krowdforce on 01/03/19.
//  Copyright © 2019 Krowdforce. All rights reserved.
//

import UIKit

class NextVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func PrevBtnAction(_ sender: UIButton) {
         performSegue(withIdentifier: "Next", sender: nil)
    }
    
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Next"{
            let abcViewController = segue.destination as? NextVC1
            abcViewController?.title = "Next1"
        }
    }
 

}

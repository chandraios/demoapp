//
//  DemoAppTests.swift
//  DemoAppTests
//
//  Created by Krowdforce on 01/03/19.
//  Copyright © 2019 Krowdforce. All rights reserved.
//

import XCTest
@testable import DemoApp

class DemoAppTests: XCTestCase {

    func testTimeAgoString(){
        let fiveMinutesAgo = Date(timeIntervalSinceNow: -5 * 60)
        let fiveMinutesAgoDIsplay = fiveMinutesAgo.timeAgoDisplay()
        XCTAssertEqual(fiveMinutesAgoDIsplay, "5 mins ago")
    }

    func testSqusreInt(){
        let value = 3
       let squareValue = value.square()
        XCTAssertEqual(squareValue, 9)
    }
}
